<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>kritsana.com</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css" />
    <style type="text/css">
    body,td,th {
    font-family: ; <!-- Chatthai -->
    }
    body {
      background-color: #0B96FF;
    }
      </style>
      <style type="text/css">
      .jumbotron {
        margin-top: 5px;
        background-image: img/head.png;
        background-size: cover;
        color: white;
      }

      </style>
    <!--[if lt IE 9]>
      <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <br>
  <div class="container">
    <nav class="navbar navbar-inverse"> <!--navbar-static-top-->
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <div class="navbar-header">
       <a class="navbar-brand" href="#">
         <img alt="Brand" src="img/coffee.png" width="26px" height="26px">
       </a>
     </div>
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">UBU CAFE</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="plateauxpage.php">หน้าเเรก <span class="sr-only">(current)</span></a></li>
        <li class="dropdown"><a href="plateauxmenu.php">เมนูอาหาร <span class="sr-only">(current)</span></a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        
        <a href="login.php" type="button" class="btn btn-danger navbar-btn"><i class="glyphicon glyphicon-user"></i> Sign in</a>
      
      </ul>
    </div>
    </div>
  </nav>
  </div>
<div class="container">
  <div class="jumbotron">
        <p class="text-center">You are Logout !!!</p>
  </div>
</div>

<div class="container">
<?php
/*
ตัวอย่างนี้จะเน้นการแยกส่วนการทำงาน
คือส่วนที่ติดต่อกับฐานข้อมูลก็จะเป็นตัวสร้างข้อมูลเพื่อส่งไปให้ template แสดงผล
โดยตัวแปรที่จะถูกใช้ใน template (inc/index.inc.php, inc/post.inc.php, inc/view.inc.php)
จะเป็นตัวแปรที่เป็นตัวพิมพ์ใหญ่ทั้งหมด เช่น $PAGE, $ITEMS
ซึ่งเราจะใช้แนวทางนี้ทั้งหมดสำหรับตัวอย่างนี้
*/

/*
ทำการเชื่อมต่อกับฐานข้อมูล ดู (inc/mysqli.inc.php)
*/
require 'inc/mysqli.inc.php';
/*
กำหนดค่า default ให้กับตัวแปร $PAGE
*/
$PAGE = empty($_GET['page'])
  ? 1
  : (int)$_GET['page'];
/*
จำนวนกระทู้ที่จะแสดงใน 1 หน้า
*/
$ITEMS_PER_PAGE = 100;
/*
คำนวณ offset เริ่มต้นที่จะกำหนดใน LIMIT ซึ่ง offset ของแถวแรกเริ่มที่ 0 ไม่ใช่ 1
ถ้าอยู่ที่หน้า 1 ก็จะได้ LIMIT 0, 100
ถ้าอยู่ที่หน้า 3 ก็จะได้ LIMIT 200, 100
*/
$START_OFFSET = ($PAGE - 1) * $ITEMS_PER_PAGE;
/*
ส่ง SQL query ไปยัง MySQL Server ด้วย mysqli::query()
หากไม่มี error และชนิดของ query ที่ส่งไปเป็น SELECT หรืออื่นๆ
ที่คืนแถวกลับมาเช่น SHOW DATABASES, SHOW VARIABLES
mysqli::query() จะ return instance ของ class mysqli_result กลับมา
นอกนั้นจะ return true หรือ false
โดยเราจะเลือก SELECT เฉพาะฟิลด์ที่ต้องใช้มาเท่านั้น
จะไม่ใช้ SELECT * เพื่อลดการใช้หน่อยความจำและเพื่อเพิ่มประสิทธิภาพ
และเราจะเรียงลำดับตามวันที่มีผู้แสดงความเห็นล่าสุด (last_commented)
*/
$result = $mysqli->query(
  "
  SELECT
    `id`,
    `created`,
    `last_commented`,
    `title`,
    `name`,
    `num_comments`,
    `last_commented_name`
  FROM `topic`
  ORDER BY `last_commented` DESC
  LIMIT {$START_OFFSET}, {$ITEMS_PER_PAGE}
  "
);
/*
เมื่อได้ $result ซึ่งจะเป็น instance ของ class mysqli_result
เราก็จะอ่านข้อมูลที่ได้ลง array ด้วย mysqli_result::fetch_assoc()
คืออ่านมาเป็น associative array และใส่เข้าไปใน array $ITEMS เพื่อนำไปใช้ใน template ต่อไป
โดยเราจะอ่านเข้ามาพักในตัวแปร $item ก่อน ซึ่งหากยังมีข้อมูลอยู่ $item จะไม่ใช่ null
จะทำให้เงื่อนไขใน while เป็นจริง $item ก็จะถูกเพิ่มเข้าไปใน $ITEMS
แต่หากอ่านข้อมูลจนหมดแล้ว (หรือไม่มีข้อมูลตั้งแต่ต้น) mysqli_result::fetch_assoc() จะ return null
และทำให้ $item เป็น null เงื่อนไขก็จะเป็นเท็จ และออกจาก while
*/
$ITEMS = array();
while ($item = $result->fetch_assoc()) {
  $ITEMS[] = $item;
}
/*
ปล่อยหน่วยความจำที่ $result ใช้
*/
$result->free();
/*
หาจำนวนกระทู้ทั้งหมด ซึ่งในที่นี้ไม่ใช้ SELECT SQL_CALC_FOUND_ROWS ร่วมกับ FOUND_ROWS()
เพราะพิสูจน์จากหลายแหล่งข้อมูลแล้วว่าทำงานช้ากว่า COUNT(*)
*/
$result = $mysqli->query('SELECT COUNT(*) FROM `topic`');
/*
mysqli_result::fetch_row() หรือ mysqli_result::fetch_assoc()
จะคืนค่ากลับมาเป็น array เสมอแม้จะมีแค่ฟิลด์เดียวที่ SELECT มาก็ตาม
แต่เนื่องจากเราอยากได้ค่าของฟิลด์แรกใน array ไม่ใช่ตัว array เอง
เราจึงใช้ current() เพื่ออ่านค่าของสมาชิกตัวแรกใน array
*/
$FOUND_ROWS = current($result->fetch_row());
/*
ปล่อยหน่วยความจำที่ $result ใช้
*/
$result->free();
/*
หาจำนวนหน้าทั้งหมด โดยหารจำนวนแถวทั้งหมดด้วยจำนวนที่แสดงผลต่อหน้า และปัดเศษขึ้นด้วย ceil()
*/
$NUM_PAGES = ceil($FOUND_ROWS / $ITEMS_PER_PAGE);
/*
บอก inc/main.inc.php ให้ require ไฟล์ inc/index.inc.php เป็น template
*/
$PAGE_TEMPLATE = 'inc/index.inc.php';
require 'inc/main.inc.php';
?>
</div>
    <script src="https://cdn.jsdelivr.net/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  </body>
</html>
